$(document).ready(function () { 
// Validacao
$.validator.setDefaults({
    submitHandler: function() {
      //alert("Formulário enviado com sucesso!");
      $('#sucessoSuperbock').modal();
      validator.resetForm();           
    }
  });

  $().ready(function() {    
    $("#cadastro-superbock").validate({
      rules: {
        pontoVenda: "required",
        responsavel: "required",        
        email: {
          required: true,
          email: true
        },
        contactoTel: {
          required: true,
          number: true
        },
        moradia: "required",
        codigoPostal: "required",
        localidade: "required",
        termos: "required"
      },
      messages: {
        pontoVenda: "Insira o nome do ponto de venda",
        responsavel: "Insira o nome do responsável",        
        contactoTel: {
          required: "Insira seu contacto telefónico",
          number: "Apenas números"
        },
        email: {
          required: "Insira um email",
          email: "Insira um e-mail válido!"
        },
        moradia: "Insira a moradia",
        codigoPostal: "Insira o código postal",
        localidade: "Insira a localidade",
        termos: "Voce precisa aceitar os termos"
      }
    });    
  });
// Mascara  
  $('#codigoPostal').mask('0000-000');
});


// Geolocalizacao

$(".btn_geo").on("click", function(e){
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(function(posicao) {
      var url = "https://nominatim.openstreetmap.org/reverse?lat="+posicao.coords.latitude+"&lon="+posicao.coords.longitude+"&format=json&json_callback=preencherDados";
      var script = document.createElement('script');
      script.src = url;
      document.body.appendChild(script);
    });
  } else {
    alert('seu navegador não suporta geolocation');
  }    
});

function preencherDados(dados) {
  document.querySelector('[name=moradia]').value = dados.address.road;
  document.querySelector('[name=localidade]').value = dados.address.city;
  document.querySelector('[name=codigoPostal]').value = dados.address.postcode;  
}